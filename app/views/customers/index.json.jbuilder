json.array!(@customers) do |customer|
  json.extract! customer, :id, :c_name, :c_address, :c_phone, :c_email
  json.url customer_url(customer, format: :json)
end
