json.array!(@slots) do |slot|
  json.extract! slot, :id, :s_name
  json.url slot_url(slot, format: :json)
end
