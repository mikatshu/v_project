json.array!(@physicians) do |physician|
  json.extract! physician, :id, :p_name, :p_phone, :p_email, :employment_id
  json.url physician_url(physician, format: :json)
end
