json.array!(@employments) do |employment|
  json.extract! employment, :id, :e_name
  json.url employment_url(employment, format: :json)
end
