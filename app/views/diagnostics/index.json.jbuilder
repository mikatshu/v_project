json.array!(@diagnostics) do |diagnostic|
  json.extract! diagnostic, :id, :d_name, :d_price
  json.url diagnostic_url(diagnostic, format: :json)
end
