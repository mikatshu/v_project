json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :customer_id, :physician_id, :app_date, :slot_id, :diagnostic_id, :app_note, :physician_note, :status_id
  json.url appointment_url(appointment, format: :json)
end
