class Physician < ActiveRecord::Base
  belongs_to :employment
  has_many :appointments
end
