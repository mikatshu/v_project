class Appointment < ActiveRecord::Base
  belongs_to :customer
  belongs_to :diagnostic
  belongs_to :physician
  belongs_to :slot
  belongs_to :status

  validates_uniqueness_of :slot_id, scope: [:physician_id, :app_date]
  validates_presence_of :customer_id, :physician_id, :app_date, :slot_id, :diagnostic_id
  before_destroy :app_change


  def app_change
    time_calc = self.slot.s_name - Time.now
    if self.app_date == Date.today and time_calc < 8
      return false
      

    end
  end
end

