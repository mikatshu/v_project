require 'open-uri'
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Diagnostic.delete_all
open("/vagrant/ike_derm_codes.csv")do|diagnose|
  diagnose.read.each_line do |diagnostic|
    d_name, d_price = diagnostic.chomp.split(",")
    Diagnostic.create!(:d_name => d_name, :d_price => d_price)
  end
end