class CreateEmployments < ActiveRecord::Migration
  def change
    create_table :employments do |t|
      t.string :e_name

      t.timestamps
    end
  end
end
