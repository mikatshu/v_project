class CreateSlots < ActiveRecord::Migration
  def change
    create_table :slots do |t|
      t.time :s_name

      t.timestamps
    end
  end
end
