class CreateStatuses < ActiveRecord::Migration
  def change
    create_table :statuses do |t|
      t.string :stat_name

      t.timestamps
    end
  end
end
