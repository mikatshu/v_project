class CreateDiagnostics < ActiveRecord::Migration
  def change
    create_table :diagnostics do |t|
      t.string :d_name
      t.decimal :d_price

      t.timestamps
    end
  end
end
