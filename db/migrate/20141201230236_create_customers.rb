class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :c_name
      t.string :c_address
      t.string :c_phone
      t.string :c_email

      t.timestamps
    end
  end
end
