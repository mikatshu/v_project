class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :customer_id
      t.integer :physician_id
      t.date :app_date
      t.integer :slot_id
      t.integer :diagnostic_id
      t.text :app_note
      t.text :physician_note
      t.integer :status_id

      t.timestamps
    end
  end
end
