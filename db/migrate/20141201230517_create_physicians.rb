class CreatePhysicians < ActiveRecord::Migration
  def change
    create_table :physicians do |t|
      t.string :p_name
      t.string :p_phone
      t.string :p_email
      t.integer :employment_id

      t.timestamps
    end
  end
end
